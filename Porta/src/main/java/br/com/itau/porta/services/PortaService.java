package br.com.itau.porta.services;

import br.com.itau.porta.exceptions.PortaNotFoundException;
import br.com.itau.porta.models.Porta;
import br.com.itau.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta getById(Long id){
        Optional<Porta> optionalPorta = portaRepository.findById(id);
        if(!optionalPorta.isPresent()){
            throw new PortaNotFoundException();
        }

        return optionalPorta.get();
    }
}
