package br.com.itau.porta.controllers;

import br.com.itau.porta.models.Porta;
import br.com.itau.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Port;
import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody @Valid Porta porta){
        return portaService.create(porta);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Porta getById(@PathVariable Long id){
        return portaService.getById(id);
    }
}
