package br.com.itau.consumer;

import br.com.itau.acesso.producer.Log;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class LogConsumer {

    @KafkaListener(topics = "spec3-arthur-jorge-1", groupId = "teste-1")
    public void receber(@Payload Log log) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        System.out.println("Recebi um log de acesso com a seguinte mensagem:" + log.getMensagem());

        Writer writer = new FileWriter("log.csv",true);
        StatefulBeanToCsv<Log> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(log);

        writer.flush();
        writer.close();


    }
}
