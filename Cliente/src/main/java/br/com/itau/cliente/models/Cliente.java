package br.com.itau.cliente.models;

import com.google.inject.internal.cglib.core.$DefaultGeneratorStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotBlank(message = "Nome é um campo obrigatório")
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
