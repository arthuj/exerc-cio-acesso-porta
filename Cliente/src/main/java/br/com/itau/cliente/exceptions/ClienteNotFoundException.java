package br.com.itau.cliente.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND,reason = "Cliente informado não existe")
public class ClienteNotFoundException extends RuntimeException{
}
