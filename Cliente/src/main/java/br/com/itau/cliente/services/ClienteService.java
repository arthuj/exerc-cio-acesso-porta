package br.com.itau.cliente.services;

import br.com.itau.cliente.exceptions.ClienteNotFoundException;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.repositories.ClienteRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorId(Long id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if(!optionalCliente.isPresent()){
            throw new ClienteNotFoundException();
        }

        return optionalCliente.get();
    }
}
