package br.com.itau.acesso.clients;

import br.com.itau.acesso.exceptions.OfflineClienteException;
import br.com.itau.acesso.models.Cliente;

public class ClienteClientFallBack implements ClienteClient{

    @Override
    public Cliente buscarClientePorId(Long id) {
        throw new OfflineClienteException();
    }
}
