package br.com.itau.acesso.clients;

import br.com.itau.acesso.exceptions.OfflinePortaException;
import br.com.itau.acesso.models.Porta;
import com.google.inject.internal.cglib.proxy.$UndeclaredThrowableException;

import javax.sound.sampled.Port;

public class PortaClientFallBack implements PortaClient {


    @Override
    public Porta getById(Long id) {
        throw new OfflinePortaException();
    }
}
