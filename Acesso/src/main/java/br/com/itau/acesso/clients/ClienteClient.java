package br.com.itau.acesso.clients;

import br.com.itau.acesso.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente-acesso", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("cliente/{id}")
    Cliente buscarClientePorId(@PathVariable Long id);
}
