package br.com.itau.acesso.models;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

public class Cliente {

    private Long id;

    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
