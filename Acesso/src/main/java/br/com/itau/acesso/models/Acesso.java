package br.com.itau.acesso.models;

import javax.persistence.*;

@Entity
@Table
@IdClass(AcessoId.class)
public class Acesso {

    @Id
    private Long porta_id;

    @Id
    private Long cliente_id;

    public Long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Long porta_id) {
        this.porta_id = porta_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
