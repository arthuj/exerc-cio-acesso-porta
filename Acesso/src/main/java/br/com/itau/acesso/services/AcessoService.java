package br.com.itau.acesso.services;

import br.com.itau.acesso.clients.ClienteClient;
import br.com.itau.acesso.clients.PortaClient;
import br.com.itau.acesso.exceptions.InvalidAcessoException;
import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.models.AcessoId;
import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;
import br.com.itau.acesso.producer.Log;
import br.com.itau.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class AcessoService {

    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    @Autowired
    private KafkaTemplate<String, Log> producer;

    public Acesso criarAcesso(Acesso acesso){
        Porta porta = portaClient.getById(acesso.getPorta_id());
        Cliente cliente = clienteClient.buscarClientePorId(acesso.getCliente_id());
        return acessoRepository.save(acesso);
    }

    public void deletarAcesso(Acesso acesso){
        Porta porta = portaClient.getById(acesso.getPorta_id());
        Cliente cliente = clienteClient.buscarClientePorId(acesso.getCliente_id());
        Optional<Acesso> optionalAcesso = acessoRepository
                .findById(new AcessoId(acesso.getPorta_id(),acesso.getCliente_id()));

        if(!optionalAcesso.isPresent()){
            throw new InvalidAcessoException();
        }

        acessoRepository.delete(acesso);
    }

    public Acesso buscarAcesso(Acesso acesso) {

        Log log = new Log();
        log.setClienteId(acesso.getCliente_id());
        log.setPortaId(acesso.getPorta_id());
        boolean randomBoolean = getRandomBoolean();
        if(randomBoolean){
            log.setMensagem("O cliente " + log.getClienteId() + " possui acesso a porta " + log.getPortaId());
            enviarAoKafka(log);
        }
        else
        {
            log.setMensagem("O cliente " + log.getClienteId() + " não possui acesso a porta " + log.getPortaId());
            enviarAoKafka(log);
        }

        Porta porta = portaClient.getById(acesso.getPorta_id());
        Cliente cliente = clienteClient.buscarClientePorId(acesso.getCliente_id());
        Optional<Acesso> optionalAcesso = acessoRepository
                .findById(new AcessoId(acesso.getPorta_id(),acesso.getCliente_id()));

        if(!optionalAcesso.isPresent()){
            throw new InvalidAcessoException();
        }

        return optionalAcesso.get();
    }

    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

    public void enviarAoKafka(Log log){
        producer.send("spec3-arthur-jorge-1", log);
    }
}
