package br.com.itau.acesso.models;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

public class Porta {

    private Long id;

    private String andar;

    private String sala;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
