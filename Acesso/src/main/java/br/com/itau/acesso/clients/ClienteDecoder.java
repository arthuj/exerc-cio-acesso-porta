package br.com.itau.acesso.clients;

import br.com.itau.acesso.exceptions.InvalidClienteException;
import br.com.itau.acesso.exceptions.InvalidPortaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder;

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidClienteException();
        }
        else{
            return errorDecoder.decode(s,response);
        }

    }
}
