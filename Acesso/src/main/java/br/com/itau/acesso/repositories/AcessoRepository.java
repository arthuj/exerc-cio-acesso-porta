package br.com.itau.acesso.repositories;

import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.models.AcessoId;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, AcessoId> {
}
