package br.com.itau.acesso.models;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import java.io.Serializable;
import java.util.Objects;

public class AcessoId implements Serializable{

    private Long porta_id;

    private Long cliente_id;

    public AcessoId() {
    }

    public AcessoId(Long porta_id, Long cliente_id) {
        this.porta_id = porta_id;
        this.cliente_id = cliente_id;
    }

    public Long getPorta_id() {
        return porta_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AcessoId)) return false;
        AcessoId acessoId = (AcessoId) o;
        return getPorta_id().equals(acessoId.getPorta_id()) &&
                getCliente_id().equals(acessoId.getCliente_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPorta_id(), getCliente_id());
    }
}
