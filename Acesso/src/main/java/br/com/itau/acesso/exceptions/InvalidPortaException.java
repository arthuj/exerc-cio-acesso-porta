package br.com.itau.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "A porta informada não existe")
public class InvalidPortaException extends RuntimeException{
}
