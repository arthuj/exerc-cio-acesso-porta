package br.com.itau.acesso.clients;

import br.com.itau.acesso.exceptions.InvalidPortaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder;

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidPortaException();
        }
        else{
            return errorDecoder.decode(s,response);
        }

    }
}
