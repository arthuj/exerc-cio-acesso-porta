package br.com.itau.acesso.controllers;

import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.services.AcessoService;
import com.google.inject.internal.cglib.core.$ClassNameReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso criarAcesso(@RequestBody @Valid Acesso acesso){
        return acessoService.criarAcesso(acesso);
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable Long clienteId,@PathVariable Long portaId){
        Acesso acesso = new Acesso();
        acesso.setCliente_id(clienteId);
        acesso.setPorta_id(portaId);
        acessoService.deletarAcesso(acesso);
    }

    @GetMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.OK)
    public Acesso buscarAcesso(@PathVariable Long clienteId,@PathVariable Long portaId){
        Acesso acesso = new Acesso();
        acesso.setCliente_id(clienteId);
        acesso.setPorta_id(portaId);
        return acessoService.buscarAcesso(acesso);
    }
}
